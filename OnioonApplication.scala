package com.onioon.util

import scala.actors.Actor
import android.content.{Context, Intent}
import android.util.Log

case class ActivityLoad(a: ActivityActor) extends Message

abstract class OnioonApplication
  extends android.app.Application
  with Actor
{
  val server : HttpServerActor
  val initMessage = scala.collection.mutable.HashMap[Class[_],Any]()

  def loadActorBase[A](decorator: Intent=>Intent)(self: Context, a: Class[A], initM: Option[Any]=None) {
    for(m <- initM) initMessage.put(a,m)
    val i = decorator(new Intent(self, a))
    self.startActivity(i)
  }

  def $l[A](a:Class[A],initM:Option[Any]=None) {loadActor(getApplicationContext,a,initM)}

  def loadActor[A](self: Context, a: Class[A], initM: Option[Any]=None) =
    loadActorBase[A]({i=>i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)})(self,a,initM)

  def loadActorNoHistory[A](self:ActivityActor, a: Class[A],initM:Option[Any]=None) {
    loadActorBase[A]({i => i})(self,a,initM)
    self.onUi {self.finish()}
  }

  override def react(handler:PartialFunction[Any,Unit]):Nothing= {
    super.react(handler orElse {
      case ActivityLoad(a) if initMessage.contains(a.getClass) => {
        a ! initMessage(a.getClass)
      }
      case x =>
        Log.w("HandlerNotFound",x.toString)
    })
  }

  this.start()
}
