package com.onioon.util

import scala.actors.Actor
import java.net.{URLEncoder}
import com.loopj.android.http._
import java.io.{File}
import android.util.Log

abstract class HttpReqParam
case class Get(url:String, params: Traversable[(String,String)], res:String=>Any) extends HttpReqParam
case class Post(url:String, params: Traversable[(String, PostParam)], res:String=>Any) extends HttpReqParam

abstract class PostParam
case class StringParam(s:String) extends PostParam
case class FileParam(f:File) extends PostParam

trait HttpServerActor extends Actor with SenderActor {
  val baseUrl:String
  private def asyncClient = new AsyncHttpClient()
  def fix[A,B](f: (A=>B)=>(A=>B)): A=>B = f(fix(f))(_)

  def generateReaction(s:PartialFunction[Any, HttpReqParam]):PartialFunction[Any,Unit] = {
    s.andThen {
      case Get(rurl,params,res) => {
        val queryString =
          if(params.isEmpty) ""
          else "?" + params
            .map((v) => URLEncoder.encode(v._1, "utf-8") + "=" + URLEncoder.encode(v._2, "utf-8"))
            .foldRight("")((x:String,y:String) => x + "&" + y)
        val url = baseUrl + "/" + rurl + queryString
        var retry=0
        fix[Unit,Unit](x => Unit =>
          asyncClient.get(url, new AsyncHttpResponseHandler {
            override def onSuccess(s:String) {
              try {res(s)}
              catch {
                case e:Exception => Log.e("Http Handling Error",url,e)
              }
            }
            override def onFailure(t:Throwable, s:String) {
              Log.e("Http Get Failed",s,t)
              if(retry<10) {
                retry += 1
                Thread.sleep(3000)
                x()
              }
            }
        }))()
      }
      case Post(rurl,params,res) => {
        val reqParams = new RequestParams()
        params.foreach({
          case (k,StringParam(_s)) => reqParams.put(k,_s)
          case (k,FileParam(f)) => reqParams.put(k,f)
        })
        var retry = 0
        fix[Unit,Unit](x => Unit => asyncClient.post(baseUrl + "/" + rurl, reqParams, new AsyncHttpResponseHandler {
          override def onSuccess(s:String) {
            try {res(s)}
            catch {
              case e:Exception => Log.e("Http Post Handling Error",rurl,e)
            }
          }
          override def onFailure(t:Throwable,s:String) {
            Log.e("Http Post Failed",s,t)
            if(retry<10) {
              retry += 1
              Thread.sleep(3000)
              x()
            }
          }
        }))()
      }
    }
  }
}

object ParamImplicits {
  implicit def string2param(s:String) = StringParam(s)
  implicit def file2param(f:File) = FileParam(f)
  implicit def listAny2listParam(l:List[(String,Any)]) =
    l.collect[(String,PostParam),List[(String,PostParam)]] {
      case (s,x:String) => (s,StringParam(x))
      case (s,x:File) => (s,FileParam(x))
    }
}