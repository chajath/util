package com.onioon.util

import android.app.{ProgressDialog, AlertDialog, Activity}
import android.os.{SystemClock, Handler, Bundle}
import scala.actors._
import android.util.Log
import android.view.{MotionEvent, View}
import android.widget.{EditText, AdapterView, TextView}
import android.view.View.OnClickListener
import android.view.animation.{AlphaAnimation, AnimationUtils}
import android.content.{Intent, Context, DialogInterface}
import android.widget.AdapterView.OnItemClickListener
import android.view.inputmethod.InputMethodManager
import android.net.Uri

case object StopMessage extends Message
case object Initialized extends Message

trait ActivityActor extends Activity with Actor with SenderActor {
  def app : OnioonApplication = this.getApplication.asInstanceOf[OnioonApplication]
  def server = app.server
  var initHandler : Unit = {}

  var handler:PartialFunction[Any, Unit] = {
    case StopMessage => {
      exit()
    }
    case Initialized => {
      initHandler
    }
  }

  def onInit(i: Unit) {
    initHandler = i
  }

  override def onCreate(savedInstanceState: Bundle) {
    super.onCreate(savedInstanceState)

    this.start()
    app ! new ActivityLoad(this)
  }
  override def onRestoreInstanceState(b:Bundle) {
    super.onRestoreInstanceState(b)

    this.start()
    app ! new ActivityLoad(this)
  }

  override def onDestroy() {
    super.onDestroy()
    this ! StopMessage
  }

  def onMessage(h: PartialFunction[Any, Unit]) {
    handler = h orElse handler
  }

  def act() {
    loop {
      try {
        react(handler)
      } catch {
        case e:Exception => {
          Log.e("ActivityActor", "Caught exception while handling message",e)
        }
      }
    }
  }

  def onUi(block : => Unit) {
    runOnUiThread(new Runnable() {
      override def run() {
        block
      }
    })
  }

  protected def $[A <: View](id: Int) = findViewById(id).asInstanceOf[A]
  protected def $t(id:Int) = $[TextView](id).getText.toString
  implicit def viewUnit2OnClickListener(u: View => Any): OnClickListener = {
    new OnClickListener {
      override def onClick(v: View) {
        v.startAnimation(AnimationUtils.loadAnimation(ActivityActor.this, com.onioon.barbie3.R.anim.onclick))
        u(v)

      }
    }
  }

  implicit def dialogUnit2OnClickListener(u: (DialogInterface, Int) => Any): DialogInterface.OnClickListener = {
    new DialogInterface.OnClickListener {
      def onClick(p1: DialogInterface, p2: Int) {u(p1,p2)}
    }
  }

  implicit def lambda2itemClick(u: (Int) => Any): OnItemClickListener = {
    new OnItemClickListener {
      def onItemClick(p1: AdapterView[_], p2: View, p3: Int, p4: Long) {
        u(p3)
      }
    }
  }

  def ->>[A <: View](id: Int, i: (A => Unit)) {
    i($(id))
  }

  def setText(strs:(Int, String)*) {
    strs.foreach({case (i,s) => $[TextView](i).setText(s)})
  }

  def alert(title: String, content: String, dismissHandler: () => Unit = () => {}) {
    val builder = new AlertDialog.Builder(this)
    builder.setMessage(content)
      .setTitle(title)
    builder.setPositiveButton("확인", (d:DialogInterface, i:Int) => {
      dismissHandler()
      d.dismiss()
    })
    builder.create().show()
  }

  def alertYesNo(title: String, content:String, yes:String, no:String, onYes: () => Unit = () => {}, onNo: () => Unit = () => {}) {
    val builder = new AlertDialog.Builder(this)
    builder.setMessage(content)
      .setTitle(title)
      .setPositiveButton(yes, (d:DialogInterface, i:Int) => {d.dismiss(); onYes()})
      .setNegativeButton(no, (d:DialogInterface, i:Int) => {d.dismiss(); onNo()})
    builder.create().show()
  }

  var progress:ProgressDialog = null

  def showProgress() {
    synchronized {
      if(progress==null || !progress.isShowing) {
        val v = new ProgressDialog(this)
        v.setMessage("Loading")
        progress = v

        progress.show()
      }
    }
  }

  def hideProgress() {
    synchronized {
      if(progress!=null && progress.isShowing) progress.dismiss()
    }
  }

  def showHide(show:Int*)(hide:Int*) {
    show.foreach(v => $[View](v).setVisibility(View.VISIBLE))
    hide.foreach(v => $[View](v).setVisibility(View.GONE))
  }

  def optionDialog(onSelect: Int => Unit, title:String, options: String*) {
    optionDialogCollection(onSelect, title, options.toTraversable)
  }

  def optionDialogCollection(onSelect: Int => Unit, title:String, options: Traversable[String]) {
    val builder = new AlertDialog.Builder(this)
    builder.setTitle(title)
      .setItems(options.toArray[CharSequence], new DialogInterface.OnClickListener() {
      def onClick(dialog:DialogInterface, which:Int) {
        onSelect(which)
      }
    })
    builder.setNegativeButton("취소",(d:DialogInterface,which:Int) => d.dismiss() )
    builder.create.show()
  }

  def hideIME(v:View) {
    val imm = getSystemService(Context.INPUT_METHOD_SERVICE).asInstanceOf[InputMethodManager]
    imm.hideSoftInputFromWindow(v.getWindowToken, 0)
  }

  def setAlphaForView(v:View, alpha:Float) {
    val animation = new AlphaAnimation(alpha, alpha)
    animation.setDuration(0)
    animation.setFillAfter(true)
    v.startAnimation(animation)
  }

  def web(url:String) {
    val i = new Intent(Intent.ACTION_VIEW,Uri.parse(url))
    startActivity(i)
  }

  def focusText(target:EditText) {
    new Handler().postDelayed(new Runnable {
      def run() {
        target.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN , 0, 0, 0))
        target.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP , 0, 0, 0))
      }
    },200)
  }
}