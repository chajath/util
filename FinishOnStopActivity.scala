package com.onioon.util

import android.app.Activity

trait FinishOnStopActivity extends Activity {
override def onStop() {
super.onStop()
finish()
}

}
