package com.onioon.util

import android.content.ContextWrapper

abstract class SettingValue
case class IntValue(v: Int) extends SettingValue
case class StringValue(v: String) extends SettingValue

abstract trait PreferenceEditor extends ContextWrapper {
  def preferenceID:String

  def setPreference(settings:(String, SettingValue)*) {
    val editor = getSharedPreferences(preferenceID, 0).edit()

    for((k, s) <- settings) s match {
      case IntValue(v) => editor.putInt(k, v)
      case StringValue(v) => editor.putString(k, v)
    }

    editor.commit()
  }

  case class getPreference[A](key:String, default: A)

  implicit def Int2IntValue(i:Int) = IntValue(i)
  implicit def String2StringValue(s:String) = StringValue(s)

  implicit def getPreference2String(g:getPreference[String]) = getSharedPreferences(preferenceID, 0).getString(g.key, g.default)
  implicit def getPreference2Int(g:getPreference[Int]) = getSharedPreferences(preferenceID, 0).getInt(g.key, g.default)
}