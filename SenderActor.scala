package com.onioon.util

import scala.actors.Actor

trait SenderActor extends Actor {
  implicit override val sender = this
}
