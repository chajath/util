package com.onioon.util

import android.app.{Activity, AlertDialog}
import android.content.DialogInterface

trait ValidationFormActivity extends ActivityActor {
  // cMatrix = (char test, weight)*
  def validLength(lengthTest: Int => Boolean, cMatrix: (Int => Boolean, Int)*)(s: String) : Boolean = {
    if(!s.forall((c)=>cMatrix.exists(_._1(c)))) return false
    lengthTest(s.map((c)=>cMatrix.filter(_._1(c))(0)._2).foldLeft(0)(_ + _))
  }

  def isHangul(c:Int) : Boolean = {
    List(
      Character.UnicodeBlock.HANGUL_SYLLABLES,
      Character.UnicodeBlock.HANGUL_JAMO,
      Character.UnicodeBlock.HANGUL_COMPATIBILITY_JAMO
    ) contains Character.UnicodeBlock.of(c)
  }

  def showError(m: String) {
    val builder = new AlertDialog.Builder(this)
    builder.setPositiveButton("확인", {(d:DialogInterface,w:Int) => d.cancel()})
    builder.setMessage(m)
    builder.show()
  }

  def validate(r:(() => Boolean, String)*):Boolean = {
    val errors = r.filter(x => ! x._1()).map(_._2)
    if(errors.isEmpty)
      true
    else {
      showError(errors.foldLeft("")(_ + "\n" + _))
      false
    }
  }
}
